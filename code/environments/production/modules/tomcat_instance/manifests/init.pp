class tomcat_instance {

  exec { 'apt-get update':
    command => '/usr/bin/apt-get update',
    onlyif  =>
      "/bin/bash -c 'exit $(( $(( $(date +%s) - $(stat -c %Y /var/lib/apt/lists/$( ls /var/lib/apt/lists/ -tr1|tail -1 )) )) <= 604800 ))'"
  }

  class { 'java':
    distribution => 'jre',
    require      => Exec['apt-get update']
  }

  tomcat::install { '/opt/tomcat':
    source_url => 'https://downloads.apache.org/tomcat/tomcat-7/v7.0.104/bin/apache-tomcat-7.0.104.tar.gz',
  }

  tomcat::instance { 'default':
    catalina_home => '/opt/tomcat',
  }
}