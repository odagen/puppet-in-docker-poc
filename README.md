1. To run docker containers `docker-compose up -d --force-recreate --build` in the directory which contains docker-compose.yaml
2. To run shell inside a container execute command `docker exec -it container_name /bin/bash` where the `container_name` one of the values:
 - puppet
 - node1
 - node2
 >For instance: `docker exec -it node1 /bin/bash`

Inside the container named *node1* or *node2* run command `sudo $(which puppet) agent -tvd` to pull puppet classes from the master node

If you get an error 'Error: Request to https://puppet:8140/puppet-ca/v1 failed after n seconds' just wait while master node will start and re-execute the command

Inside the 'code' directory resides puppet code which describes the state of a node agent

As result, you will have nginx instance listening on port 8800 and tomcat instance on port 9900 of your host OS:
http://localhost:8800 - nginx
http://localhost:9900 - tomcat
